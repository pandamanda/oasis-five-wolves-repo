label prologue:
    # "Orion Agency, as one of the biggest private investigation company, was no stranger for spaceship investigation job."
    # "There were quite a lot of cases in which spaceships lost contacts and were found abandoned, either all of their crews were died due to an unfortunate accident or, in some rare cases, left behind."
    # nvl clear
    # "It was 3rd February 3035 when Orion Agency approved a spaceship investigation request by Elon Stellar Mining."
    # "Elon Stellar Mining was one of Orion's frequent client, especially for spaceship investigation since the mining company owned the largest non-military spaceship fleet on Earth Federation."
    # "For this latest request, the job was assigned to Thebes-01 branch. They were the closest branch to the spaceship's location."
    # nvl clear
    # "They did own a bunch of competent agents, but for this one time .... Due to availability, Thebes-01 branch office had to send two agents that had no prior experience handling an {i}actual{/i} spaceship investigation."
    # "After a 23 hours journey, the investigator's ship arrived at their destination a few miles outside Pluto's orbit."
    # "The smaller ship, piloted by an Elon Stellar Mining crew, slowly flew closer to a bigger spaceship with a proud Elon Stellar Mining logo etched on its body."
    # "The spaceship number was AH-255, also known as \"Hart 255\". All company's expedition spaceships were named after famous adventurer or explorer. This one was no exception."
    # nvl clear
    # "As the investigation ship tried to initiate an encrypted connection, the two private investigators reviewed their job."

    centered "3rd February 3035"
    centered "A few miles outside Pluto's orbit."
    centered "Inside Flamingo-11, investigation ship registered under Elon Stellar Mining Company."
    
    mik "I'm surprised you have a license to navigate a small spaceship."
    ade "Will you believe me if I say I'm good with vehicles? In terms of driving them."
    mik "Well ... I've seen you driving a car before, so I can believe that claim."
    mik "But, really, from all spaceship the company offered to us, you chose this one?"
    mik "Its navigation system is barebone and a lot of things have to be handled manually."

    ade "B-but it's sufficient! It's decent enough."
    ade "I don't understand why the brand new spaceships have to install those smart AIs or have a humanoid navigator droid."
    mik "...."
    mik "Alright, alright. I know you don't like to speak with AIs."
    mik "These manual system are a bit impractical for me."

    mik "It'll take some time before proper connection to target is established."
    mik "Should we go through the briefing once more?"
    ade "Please do."
    ade "... That long sleep we've put through makes me feel slow."
    mik "That's the goal of those long sleeps during space travel, to slow down the processes in your body so you don't spend too much unnecessary energy and don't need to eat or drink to replenish those energy."
    ade "I know, I know."
    ade "I just don't like the slow feeling after waking up."
    ade "Anyway. The briefing."

    #Something to show the briefing
    mik "Job request number 3900. Accepted by Orion Agency on 29th January 3035 and assigned to Thebes-01 branch on 31st January 3035 after reviewing processes."
    mik "Client: Elon Stellar Mining. Type: spaceship investigation."
    mik "Spaceship Hart-255 was launched back in 21st May 3030 as part of regular scouting expedition, led by Captain Edward James Falklend and navigation AI Sawyer."
    mik "Its objective was to find and locate any planets or asteroids rich of minerals."

    ade "There was a protest by Green Planet group on the day of the launch, right?"
    mik "... Was there? I don't remember."
    ade "You need to watch more news."
    ade "Please continue, by the way."

    mik "Elon Stellar Mining lost contact with Hart-255 on 4th November 3033 and every effort to contact the spaceship was failed. A year after that, they reported the spaceship as missing, but due to the ship's last location no search attempt initiated."
    mik "The ship's last location was around ten lightyears from Earth."
    ade "It's the uncharted region?"
    mik "Yes. Sending a search team to uncharted region cost a lot of money with very low chance of success."

    ade "Just out of curiosity, how low is it?"
    mik "The chance of finding lost contact spaceship in uncharted region? It's ... around 0.000095\%\."
    ade "Wow. No wonder the company prefer to pay compensation for the spaceship crews family rather than sending a search team."
    mik "It's just logical."
    mik "Elon Stellar Mining Europa base received a message from Hart-255 around 1st January 3035. They tried to contact the spaceship, but receive no answer, so they tracked the ship's location."
    mik "The ship moved and hyperjumped two times before stayed still on its current location near Pluto."

    ade "I hate to spell this out, but ...."
    mik "Spell it out."
    ade "It's your typical ghost ship plot."
    mik "Adel.{w} Please. {w}There's no such thing as ghost."
    mik "It's 3035 now."

    ade "Ghost don't age and they don't care about years."
    mik "The only thing close to your so called \"ghost\" is an uploaded consciousness."
    #notification sound
    mik "Oh. {w}Connection established."
    ade "Great! Now let's get to work."
    mik "(Is it just me or Adel looks a bit anxious after mentioning ghost ship plot?)"

    #transition
    ade "Hart-255, can you hear us?"
    ade "....{w} No response."
    mik "Try again. Why are you looking at me like that?"
    ade "N-nothing."
    mik "You're thinking about ghost ship again."

    ade "Erm ...."
    mik "...."

    mik "Adel, just try contacting the ship again, please."
    ade "Can't we use the root access code from the company? They allow us to use it to gain access to the ship. It'd be easier for the investigation."
    mik "...."
    ade "Alright ... I'll try again."
    ade "I'm not too positive about this. The ship's crews might be already ... well, gone"

    ade "Hart-255, can you hear us?"
    ade "..."
    extend "..."
    extend "..."
    ade "(I hate being ignored like this ....)"
    ade "Hart-2--"

    unk "Greetings!"
    unk "Sawyer of Hart-255 at your service!"
    ade "Greetings, Sawyer."
    ade "I'm Adelynn Stark from Orion Agency, currently onboard Flamingo-11 sent as per Elon Stellar Mining company request."
    ade "I'd like to inquire the current situation aboard Hart-255. Did something happen so you couldn't return call from the company?"

    saw "There had been a ... complication."
    ade "Engine trouble?"
    saw "A little engine trouble, a lot of crew trouble."
    saw "Sawyer would like to ask for your assistance regarding the trouble."
    ade "How could we help you?"

    saw "Well ...."
    saw "You can start by figuring out who murdered me."
    mik "You're not the AI Sawyer, as suspected."
    saw "Since when did you know?"

    mik "From the very beginning."
    mik "You replied with a non-standard greetings for a navigation AI."
    mik "And then you paused a little when answering question, which is a trait for human speech not an AI."
    ade "Human speech?"
    ade "You mean I'm talking with a ...."

    mik "An uploaded consciousness."
    saw "You're a smart one. I'm impressed the company send someone capable this time."
    saw "I noticed how you didn't immediately use the root access code. If you used it, it might cause another trouble for both of us."
    ade "Mika? Kindly explain to me?"

    mik "The root access code would work fine if the navigation AI is still in place, but since it's already gone and replaced, the code most likely would be rejected and might cause fatal errors."
    saw "That is correct."
    saw "Now, investigators, please, do me a favor and figure out who murdered me."
    mik "You're in no place for bargain."

    saw "Figure out who murdered me or say good bye to Thebes-01."
    mik "What?"
    saw "Thebes-01 is in line with this ship next hyperjump position. You know very well what happened to objects such as a space station that stands in line with a spaceship hyperjump route."
    mik "You've just said a very serious threat that is enough for the authorities to make a move."
    saw "I have to."
    ade "And why is that?"
    saw "As you've said earlier, all crews on Hart-255 had died due to fatal malfunction."
    saw "However, it's easy for the company to just sign this as an accident and decide not to take any action for what they've done to me."
    saw "My physical body might be dead. The only thing left of me is this ... incomplete virtual mind."

    saw "But I want to at least know who murdered me and why before being erased."
    ade "I see. That's a good point."
    mik "We didn't sign up for this mess ...."
    ade "I don't see any other way out of this situation. If you have any suggestion, do tell."
    mik "Cannot think of one for now."
    mik "...."
    mik "Alright. We'll take this case then."

    return