﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define eil = Character("Eileen")

define ade = Character("Adel")
define mik = Character("Mika")

define unk = Character("???")
define saw = Character("Sawyer")

#define narrator = nvl_narrator


# The game starts here.

label start:

    jump prologue
